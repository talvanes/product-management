<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Products', function (Blueprint $table) {
     	       	$table->increments('id');
		$table->unsignedInteger('categoryId');
		$table->foreign('categoryId')->references('id')->on('Categories');
		$table->string('name');
		$table->text('description');
		$table->decimal('price', 8, 2)->default(0.00);
		$table->decimal('stock')->default(0.0);
		$table->boolean('special')->default(0);
            	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Products');
    }
}
