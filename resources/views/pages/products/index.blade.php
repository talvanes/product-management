@extends('layouts.master')

@section('title', "Product list")

@section('content')
	<h1>Product list</h1>
	<p>Here is the product list. <a href="{{ route('products.create') }}">Do you want to create a new one?</a></p>
	<hr />
@stop
