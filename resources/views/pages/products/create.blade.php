@extends('layouts.master')

@section('title', "New product")

@section('content')

	<aside class="w3-container">
		<header class="w3-container">
			<h1>Add Product</h1>
		</header>

		<main class="w3-container">
			<!-- Create Form -->
			{!! Form::open([
				'route' => 'products.store',
				'method' => 'POST'
			]) !!}

			<!-- All the form inputs here -->
			@include('partials.form.products.inputs')

			<br />

			<div class="w3-row">
				{!! Form::submit('Create Product', ['class' => 'w3-btn w3-round w3-blue']) !!}
			</div>
			<!-- End of form inputs -->

			{!! Form::close() !!}
		</main>

	</aside>
@stop
