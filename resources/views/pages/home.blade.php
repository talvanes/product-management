@extends('layouts.master')

@section('title', "Home Page")

@section('content')
	<h1>Welcome home</h1>
	<p>Lorem ipsum dolor sit amet...</p>

	<hr />
	<a href="{{ route('products.index') }}" class="w3-btn w3-round w3-aqua">View products</a>
	<a href="{{ route('products.create') }}" class="w3-btn w3-round w3-blue">Add product</a>
@stop
