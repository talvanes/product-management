<!doctype html>
<html>
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>App Name - @yield('title')</title>
		<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css" />
	</head>

	<body>
		<header>
			<ul class="w3-navbar w3-border w3-round w3-light-grey">
				<li><a href="{{ route('home') }}">Brand</a></li>
				<li class="w3-right"><a href="#">Categories</a></li>
				<li class="w3-right"><a href="{{ route('products.index') }}">Products</a></li>
				<li class="w3-right"><a href="{{ route('home') }}">Home</a></li>
			</ul>	
		</header>

		<main>
			<div class="w3-container">
				@yield('content')
			</div>
		</main>
	</body>
</html>
