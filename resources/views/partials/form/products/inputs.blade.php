<div class="w3-row">

	<!-- Name -->
	<div class="w3-container">
		{!! Form::label('name', 'Name: ', ['class' => 'w3-label w3-validate']) !!}
		{!! Form::text('name', null, ['class' => 'w3-input']) !!}
	</div>

</div>


<div class="w3-row">

	<!-- Price -->
	<div class="w3-container w3-col" style="width: 15%;">
		{!! Form::label('price', 'Price: ', ['class' => 'w3-label w3-validate']) !!}
		{!! Form::number('price', '0', ['class' => 'w3-input', 'min' => 0, 'step' => 0.01]) !!}
	</div>

	<!-- Stock -->
	<div class="w3-container w3-col" style="width: 15%;">
		{!! Form::label('stock', 'Stock: ', ['class' => 'w3-label w3-validate']) !!}
		{!! Form::number('stock', '0', ['class' => 'w3-input', 'min' => 0, 'step' => 0.01]) !!}
	</div>

	<!-- Special? -->
	<div class="w3-container w3-col" style="width: 10%;">
		{!! Form::label('special', 'Special ', ['class' => 'w3-label w3-validate']) !!}
		<br />
		{!! Form::checkbox('special', '0', false, ['class' => 'w3-check']) !!}
	</div>

	<!-- TODO: Select Box: Categories -->
	<div class="w3-container w3-rest">
		{!! Form::label('categoryId', 'Categories ', ['class' => 'w3-label w3-validate']) !!}
		<br />
		{!! Form::select('categoryId', ['P' => 'little', 'M' => 'medium', 'G' => 'big'], null, ['class' => 'w3-input', 'placeholder' => 'Select a category...']) !!}
	</div>

</div>


<div class="w3-row">

	<!-- Description -->
	<div class="w3-container">
		{!! Form::label('description', 'Description: ', ['class' => 'w3-label w3-validate']) !!}
		{!! Form::textarea('description', null, ['class' => 'w3-input']) !!}
	</div>

</div>

