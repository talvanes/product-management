<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'Categories';
	protected $fillable = [
		'parentId',
		'name',
	];
	public static $snakeAttributes = false;
}
